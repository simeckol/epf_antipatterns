# Popis anti-patternů pomocí EPF

## Artefakty
Artefakty (formální) jsou reprezentovány jako *Work Product/Artifact*, každý má zároveň přiřazený *Task*, jehož je produktem.

Lze u nich specifikovat atribut *keywords*, podle kterého se poté nacházejí v databázi.

Dále lze specifikovat atribut *amount* udávající podíl takovýchto artefaktů vůči ostatním (většinou všem).

Roli, která je autorem artefaktu, je nutné nastavit jako vykonavatele příslušného *Tasku*.

## Commity
Commity lze reprezentovat jako *Work Product/Outcome* s nastaveným atributem *type=commit*.

Je možné u nich specifikovat atributy *keywords* a *amount*.

## Úkoly

Samotné úkoly jsou reprezentovány jako *Task* bez výstupních artefaktů.

Je možné u nich specifikovat atributy *keywords* a *amount*, případně v záložce *Roles* přiřadit vykonavatele.

## Ostatní
V EPF nelze přehledně vyznačit neexistenci něčeho. Anti-patterny, které tuto vlastnost potřebují, jsou definovány jako *Patterny* atributem *pattern=true* v *Plug-in/Brief Description* a jsou modelovány jako patterny. I jejich vyhodnocení je poté invertované.
